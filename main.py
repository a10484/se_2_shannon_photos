import image_proccess
from texttable import Texttable
from PIL import Image

exit_file = "final-result"
info_imgs = [['image', 'H', "H'", 'e', "e'", 'p', "p'", 'n_bar', 'H_max', 'Image size', 'mage size(after coding)', 'Compression ratio']]

for i in range(6):  # 6
    path = f"bmp/{i + 1}.bmp"
    image_file = Image.open(path)
    image_show = Image.open(path)
    # image_show.show()
    print()
    print(f"Info image {i + 1}.bmp")
    size_file = str(len(image_file.fp.read()))
    additional_info = path + " => " + "File Size In Bytes: " + size_file
    print(additional_info)
    info_imgs.append(image_proccess.proccess(path=path, G_name=f"G-{i + 1}", additional_info=additional_info, exit_file=exit_file, size_file=size_file))

print()
print("=============== info for all images ===============")
print()

info_table=Texttable()
# table.set_deco(Texttable.VLINES)
info_table.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", 'c'])
info_table.set_cols_valign(["m", "m", "m", "m", "m", "m", "m", "m", "m", "m", "m", 'm'])
info_table.set_cols_width(["5", "5", "5", "5", "5", "5", "5", "5", "5", "6", "10", "5"])
info_table.add_rows(info_imgs)

# Open a file with access mode 'a'
with open(exit_file + ".txt", "a") as file_object:
    # Append 'hello' at the end of file
    file_object.write("\n\n\n")
    file_object.write("=============== info for all images ===============")
    file_object.write("\n\n")
    file_object.write(info_table.draw())
    file_object.write("\n")

print(info_table.draw())

print()
