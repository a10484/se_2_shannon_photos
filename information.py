import math


def information(sorted_count_repeated_elements, probabilities_elements, place=4):
    H_entropy = 0
    # code_word_lengths = sorted_count_repeated_elements.copy()
    cumulative_probabilities = sorted_count_repeated_elements.copy()
    cumulative_probabilitie = 0

    for key in probabilities_elements:
        information = round(-math.log(probabilities_elements[key], 2), place)
        # code_word_lengths[key] = math.ceil(information)
        cumulative_probabilities[key] = cumulative_probabilitie
        cumulative_probabilitie += probabilities_elements[key]
        H_entropy += (probabilities_elements[key] * information)

    return {'H_entropy': H_entropy, "cumulative_probabilities": cumulative_probabilities}
