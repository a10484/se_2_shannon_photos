# importing required libraries of opencv
import math
import cv2
# importing library for plotting
from matplotlib import pyplot as plt

import numpy as np
import information as img_info
from texttable import Texttable
import float2binary as f2b


def proccess(path, G_name, additional_info, exit_file, size_file):
    # load an image in *****grayscale**** mode
    G = cv2.imread(path, 0)
    # print(G)  # <class 'numpy.ndarray'>
    h, w = G.shape

    # calculate frequency of pixels in range 0-255
    histg = cv2.calcHist([G], [0], None, [256], [0, 256])

    unique, counts = np.unique(G, return_counts=True)

    zip_iterator = zip(unique, counts)
    sorted_count_repeated_elements = dict(zip_iterator)
    probabilities_elements = {
        key: sorted_count_repeated_elements[key] / (h * w) for key in sorted_count_repeated_elements.keys()}

    # show the plotting graph of an image
    plt.plot(histg)
    plt.show()

    info = img_info.information(
        sorted_count_repeated_elements, probabilities_elements, 20)

    H_max = round(math.log(len(probabilities_elements), 2), 4)
    H_entropy = info['H_entropy']
    cumulative_probabilities = info['cumulative_probabilities']
    efficiency = H_entropy / H_max
    redundancy = 1 - efficiency

    print()
    # https://pypi.org/project/texttable/
    table = Texttable()
    # table.set_deco(Texttable.VLINES)
    table.set_cols_width(["3", "7", "13", "13", "5", "16", "6"])
    table.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
    table.set_cols_valign(["m", "m", "m", "m", "m", "m", "m"])
    table.set_cols_dtype(['a', 'a', 'a', 'a', 'a', 't', 'a'])
    table_content = [['row', 'Symbols', 'Probabilities',
                      'Information', 'f_i', 'codewords', "n_i"]]

    i = 1
    n_bar = 0

    for key in probabilities_elements:
        information = round(-math.log(probabilities_elements[key], 2), 4)
        n_bar += probabilities_elements[key] * math.ceil(information)
        try:
            row = [i, key, probabilities_elements[key],
                   f"{information} bit", cumulative_probabilities[key], f2b.float_bin(float(cumulative_probabilities[key]), places=math.ceil(information))[1:], math.ceil(information)]
        except:
            print("----------------------------")
            print(f"error => {key}")
            print("----------------------------")
            row = [i, key, probabilities_elements[key],
                   f"{information} bit", cumulative_probabilities[key], "0" * math.ceil(information), math.ceil(information)]
        table_content.append(row)
        i += 1

    # f2b.float_bin(float(cumulative_probabilities[key]), places=math.ceil(information))

    table.add_rows(table_content)

    # Open a file with access mode 'a'
    with open(exit_file + ".txt", "a") as file_object:
        # Append 'hello' at the end of file
        file_object.write("\n\n")
        file_object.write(str(additional_info))
        file_object.write("\n")
        file_object.write(table.draw())
        file_object.write("\n")

    print(table.draw())

    print()

    e_prime = H_entropy/n_bar

    info_table = Texttable()
    # table.set_deco(Texttable.VLINES)
    info_table.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c"])
    info_table.set_cols_valign(["m", "m", "m", "m", "m", "m", "m", "m"])
    info_table.add_rows([['H', "H'", 'e', "e'", 'p', "p'", 'n_bar', 'H_max'], [
                        H_entropy, e_prime, efficiency, e_prime, redundancy, 1 - e_prime, n_bar, H_max]])

    # Open a file with access mode 'a'
    with open(exit_file + ".txt", "a") as file_object:
        # Append 'hello' at the end of file
        file_object.write("\n")
        file_object.write(info_table.draw())
        file_object.write("\n")

    print(info_table.draw())

    return [G_name, H_entropy, e_prime, efficiency, e_prime, redundancy, 1 - e_prime, n_bar, H_max, size_file, (float(size_file) * (1 - e_prime)), 1 - e_prime]
